#include "common.h"

int main()
{

	//Filename is ingredients.txt
	std::string fileName = "ingredients.txt";
	
	int input;
	//Menu Item
	do{
		std::cout << "Health Concerns" << std::endl;
		std::cout << "1. High Cholesterol" << std::endl;
		std::cout << "2. Low Calorie" << std::endl;
		std::cout << "3. Replace Everything" << std::endl;
		std::cout << "4. Don't Modify" << std::endl;
		std::cout << "0. Exit" << std::endl;
		std::cin >> input;
		//if input is valid run program
		if(input > 0 && input < 5)
		{
			Recipe recipe(fileName);
			recipe.suggest(input);
		}
	}while(input != 0);
}