#pragma once
#include "common.h";
class Recipe
{
private:
	std::string fileName;

public:
	Recipe(std::string);
	Recipe(void);
	~Recipe(void);
	void suggest(int);
	void print();
};

