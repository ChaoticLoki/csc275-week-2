#pragma once
#include "common.h"
class Ingredient
{
private:
	double quantity;
	std::string unit;
	std::string ingredient;
public:
	Ingredient(void);
	Ingredient(double,std::string,std::string);
	~Ingredient(void);
	void suggest();
	void print();
};

