#include "common.h"


Ingredient::Ingredient(void)
{
}

//Initialize variables
Ingredient::Ingredient(double quantity,std::string unit,std::string ingredient)
{
	this->quantity = quantity;
	this->unit = unit;
	this->ingredient = ingredient;
}

//Print out line without changing anything
void Ingredient::print()
{
	std::cout << this->quantity << " " << this->unit << " " << this->ingredient << std::endl;
}

//Convert quantities to appropriate value, leave unit the same as the input
void Ingredient::suggest()
{
	if(this->ingredient == "sour cream")
	{
		std::cout << this->quantity << " " << this->unit << " " << "yogurt" << std::endl;
	}
	else if(this->ingredient == "milk")
	{
		double conversion = this->quantity / 2;
		std::cout << conversion << " " << this->unit << " " << "evaporated milk and " << conversion << " " << this->unit << " water" << std::endl;
	}
	else if(this->ingredient == "lemon juice")
	{
		double vinegar = this->quantity / 2;
		std::cout << vinegar << " " << this->unit << " vinegar" << std::endl;
	}
	else if(this->ingredient == "sugar")
	{
		double honey = this->quantity / 2, molasses = this->quantity, agave = this->quantity / 4;
		std::cout << honey << " " << this->unit << " honey, " << molasses << " " << this->unit << " molasses, " << agave << " " << this->unit << " agave nectar" << std::endl;
	}
	else if(this->ingredient == "butter")
	{
		std::cout << this->quantity << " " << this->unit << " " << "yogurt" << std::endl;
	}
	else if(this->ingredient == "flour")
	{
		std::cout << this->quantity << " " << this->unit << " " << "rice flour" << std::endl;
	}
	else if(this->ingredient == "mayonnaise")
	{
		std::cout << this->quantity << " " << this->unit << " " << "cottage cheese" << std::endl;
	}
	else if(this->ingredient == "egg")
	{
		double cornstarch = this->quantity * 2;
		std::cout << cornstarch << " " << "tablespoons" << " " << "cornstarch" << std::endl;
	}
	else if(this->ingredient == "oil")
	{
		std::cout << this->quantity << " " << this->unit << " " << "applesauce" << std::endl;
	}
	else
	{
		this->print();
	}
}

Ingredient::~Ingredient(void)
{
}
