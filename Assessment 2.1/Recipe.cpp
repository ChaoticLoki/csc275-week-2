#include "common.h"

Recipe::Recipe(void)
{
}

Recipe::Recipe(std::string fileName)
{
	this->fileName = fileName;
}
void Recipe::print()
{
	std::cout << "----Ingredients----" << std::endl;
	std::ifstream file(this->fileName);
	std::string line;
	while(getline(file, line))
	{
		std::cout << line << std::endl;
	}
}


void Recipe::suggest(int input)
{
	std::ifstream file(this->fileName);
	std::string line;
	std::cout << "----Suggestion----" << std::endl;
	//Get each line of file
	while(getline(file, line))
	{
		std::stringstream stringStream(line);
		double quantity;
		std::string unit;
		std::string ingredient;
		//Parse each line into variables
		stringStream >> quantity >> unit;
		getline(stringStream, ingredient);
		//Eliminate first whitespace
		ingredient.erase(0, 1);
		//Create an instance of class Ingredient
		Ingredient singleIngredient(quantity, unit, ingredient);
		//Check user's input
		switch(input)
		{
			case 1:
				//Low cholesterol
				if (ingredient == "egg" || ingredient == "milk" || ingredient == "butter" || ingredient == "mayonnaise")
					singleIngredient.suggest();
				else
					singleIngredient.print();
				break;
			case 2:
				//low calorie
				if(ingredient == "mayonnaise" || ingredient == "sour cream" || ingredient == "oil")
					singleIngredient.suggest();
				else
					singleIngredient.print();
				break;
			case 3:
				//suggest Everything
				singleIngredient.suggest();
				break;
			case 4:
				//just print everything
			default:
				singleIngredient.print();
		}
	}
	std::cout << std::endl;
}

Recipe::~Recipe(void)
{
}
