#include "common.h"

//Print out Array
void print(int *input, int size)
{
	for (int index = 0; index < size; index++)
	{
		if(index == size-1)
		{
			std::cout << input[index]; 
		}
		else
		{
			std::cout << input[index] << ", "; 
		}
	}
	std::cout << std::endl;
}

void QuickSort(int inputArray[], int leftPointer, int rightPointer)
{
	int left = leftPointer, right = rightPointer;
	//Find pivot element of array 
	int pivot = inputArray[(leftPointer + rightPointer) / 2];

	//While the left pointer is not bigger than the right pointer compare each element
	while (left <= right)
	{
		// While the element in the left pointer is less than the pivot, increment the pointer because the value is less than the pivot and therefore in the right area.
		while(inputArray[left] < pivot)
			left++;
		//While the element in the right pointer is greater than the pivot, decrement the pointer because the value is greater than the pivot and therefore in the right area.
		while(inputArray[right] > pivot)
			right--;
		//If the left pointer is less than or equal to the right pointer, swap the elements to place them in order
		if(left <= right)
		{
			int tmp = inputArray[left];
			inputArray[left] = inputArray[right];
			inputArray[right] = tmp;
			left++;
			right--;
		}
	}
	print(inputArray, 10);
	//If the left pointer is less than this instance of QuickSort's left pointer, run Quicksort again.
	if (leftPointer < left)
		QuickSort(inputArray, leftPointer, right);
	//If this QuickSort's instance of the left pointer is less than the right pointer, run QuickSort again.
	if (left < rightPointer)
		QuickSort(inputArray, left, rightPointer);
}

int main()
{
	std::cout << "Input 10 different numbers seperated by a space: ";
	int input[10];
	for(int index = 0; index < 10; index++)
	{
		std::cin >> input[index];
	}
	print(input, 10);
	QuickSort(input, 0, 9);
	int temp;
	std::cin >> temp;
}